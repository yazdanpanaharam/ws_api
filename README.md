# ws_api

wsCore
=====


Client server WebScoket Api
=====

define type of Api Type:
----
Clinet ----> Server (type = Request)<br>
Server ----> Clinet(type = Reply) --> when clinet send Request Server Reply That<br>

Server ----> Clinet (type = signal) --->  Server send to client whenever it wishes (on demand)

WebSocket Connect API
----
ws://171.22.27.153:8585/$session_id/$user_id <br>
Header:
"Authuraization": "Beare UserAccessToken"



Common API
====
GetAuthKey
----
in the first of all:<br>
if client established ws connection must call GetAuthKey Api and receive AuthKey<br>
after that client can call other api<br>
Note: <br>
received Auth key must be insert in all api that client will call<br>
for every new ws connection new AuthKey generate for client

type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|      AuthKey       | InternalServerError      |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": null,
            "Verb": "GetAuthKey"},
 "Body" : {}}
```
AuthKey
----
type: Reply
```json
{"Header": {"RequestID": 'int',
            "Verb": "AuthKey"},
 "Body" : {"AuthKey" :  'string'}
 }
```
> Example:
>> Request: 
>> ``` json 
>> {"Header": {"RequestID": 123456987,
>>            "AuthKey": null,
>>            "Verb": "GetAuthKey"},
>> "Body" : {}}
>> ```
>> Reply:
>> ``` json 
>> {"Header": {"RequestID": 123456987,
>>            "Verb": "AuthKey"},
>> "Body" : {"AuthKey" : "KLEabctgejh2Ayhe3IUEND"}
>> }
>> ```


GetSessionInfo
----
type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|      SessionInfo   |       AuthError          |
|                    |  InternalServerError     |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": 'string',
            "Verb": "GetSessionInfo"},
 "Body" : {}}
```

SessionInfo
----
type: Reply
```json
{"Header": {"RequestID": 'int',
            "Verb": "SessionInfo"},
 "Body" : {"Documents": [{"DocID": 'string',
                           "Title" :  'string'}],
           "ConferenceInfo": {"SipIP": 'string',
                              "SipPort":  'int',
                              "UserID":  'string',
                              "Pass":  'string',
                              "DialedNum": 'string'},
           "Members": [{"Name": 'string',
                        "UserID": 'string',
                        "Avatar": 'string',
                        "Status" : 'ONLINE | OFFLINE',
                        "ConferenceStatus": 'ONLINE | OFFLINE',
                        "RoleInSession": 'MAIN_REVIEWER | PROFESSOR | RESEARCHER | REVIEWER'}],
           "ActiveDocumentID": 'string',
           "YourRoleInSession": 'MAIN_REVIEWER | PROFESSOR | RESEARCHER | REVIEWER',
           "SessionStatus": {"Recording": 'boolean'},
           }
 }
```
> Example:
>> Request: 
>> ``` json 
>> {"Header": {"RequestID": 74582369,
>>            "AuthKey": "KLEabctgejh2Ayhe3IUEND",
>>            "Verb": "GetSessionInfo"},
>> "Body" : {}}    
>> ```
>> Reply:
>> ``` json
>> {"Header": {"RequestID": 74582369,
>>            "Verb": "SessionInfo"},
>> "Body" : {"Documents": [{"DocID": "testDocIDExample",
>>                           "Title" :  "testDoc.pdf"}],
>>           "ConferenceInfo": {"SipIP": "130.185.76.61",
>>                              "SipPort":  5062,
>>                              "UserID":  "5d0269b050f324328dd6423b",
>>                              "Pass":  "5236148",
>>                              "DialedNum": "987654"},
>>           "Members": [{"Name": "ali",
>>                        "UserID": "5d24d2c4eb2efc715c152151",
>>                        "Avatar": null,
>>                        "Status" : 'ONLINE',
>>                        "ConferenceStatus": 'OFFLINE',
>>                        "RoleInSession": "PROFESSOR"}],
>>           "ActiveDocumentID": "exampleDoc",
>>           "YourRoleInSession":  "RESEARCHER",
>>           "SessionStatus": {"Recording": true},
>>           }
>> }  
>> ```

GetMembers
----
type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|      Members       |       AuthError          |
|                    |  InternalServerError     |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": 'string',
            "Verb": "GetMembers"},
 "Body" : {}}
```

Members
----
type: Reply
```json
{"Header": {"RequestID": 'int',
            "Verb": "Members"},
 "Body" : {"Members": [{"Name": 'string',
                        "UserID": 'string',
                        "Avatar": 'string',
                        "Status" : 'ONLINE | OFFLINE',
                        "ConferenceStatus": 'ONLINE | OFFLINE',
                        "RoleInSession": 'MAIN_REVIEWER | PROFESSOR | RESEARCHER | REVIEWER'}]
           }
 }
```
> Example:
>> Request: 
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "AuthKey": "KLEabctgejh2Ayhe3IUEND",
>>            "Verb": "GetMembers"},
>> "Body" : {}} 
>> ```
>> Reply:
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "Verb": "Members"},
>> "Body" : {"Members": [{"Name": "ali",
>>                        "UserID": "5d0269b050f324328dd6423b",
>>                        "Avatar": null,
>>                        "Status" : "ONLINE",
>>                        "ConferenceStatus": "ONLINE",
>>                        "RoleInSession": "REVIEWER"}]
>>           }
>> } 
>> ```

PublishMsg
----
type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|      Done          |       AuthError          |
|                    |  InternalServerError     |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": 'string',
            "Verb": "PublishMsg"},
 "Body" : {
          "MsgType": 'TEXT',
          "MsgBody": 'string'
 }}
```
> Example:
>> Request: 
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "AuthKey": "KLEabctgejh2Ayhe3IUEND",
>>            "Verb": "PublishMsg"},
>> "Body" : {}} 
>> ```
>> Reply:
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "Verb": "Done"},
>> "Body" : {} 
>> ```


LeftSession
----
type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|      Done          |       AuthError          |
|                    |  InternalServerError     |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": 'string',
            "Verb": "LeftSession"},
 "Body" : 
      "Reason": 'string | null'
 }}
```
> Example:
>> Request: 
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "AuthKey": "KLEabctgejh2Ayhe3IUEND",
>>            "Verb": "LeftSession"},
>> "Body" : {}} 
>> ```
>> Reply:
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "Verb": "Done"},
>> "Body" : {} 
>> ```


Done
----
type: Reply
```json
{"Header": {"RequestID": 'int',
            "Verb": "Done"},
 "Body" : {}
 }
```

Error
----
type: Reply
```json
{"Header": {"RequestID": 'int',
            "Verb": "Error"},
 "Body" : {
        "ErrType" : 'string',
        "Description" : 'string'
 }
 }
```

Presenter API
====
ChangeActiveDoc
----
type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|       Done         |       AuthError          |
|                    |  InternalServerError     |
|                    |    PermissionDenied      |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": 'string',
            "Verb": "ChangeActiveDoc"},
 "Body" : {
      "direction": 'NEXT | PREVIOUS'
 }}
```
> Example:
>> Request: 
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "AuthKey": "KLEabctgejh2Ayhe3IUEND",
>>            "Verb": "ChangeActiveDoc"},
>> "Body" : {"direction": "NEXT"}} 
>> ```
>> Reply:
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "Verb": "Done"},
>> "Body" : {} 
>> ```


Admin API
====
FormalizeSession
----
type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|      Done          |       AuthError          |
|                    |  InternalServerError     |
|                    |    PermissionDenied      |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": 'string',
            "Verb": "FormalizeSession"},
 "Body" : {}}
```
> Example:
>> Request: 
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "AuthKey": "KLEabctgejh2Ayhe3IUEND",
>>            "Verb": "FormalizeSession"},
>> "Body" : {}} 
>> ```
>> Reply:
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "Verb": "Done"},
>> "Body" : {} 
>> ```

EndSession
----
type: Request
```
|      sucess        |       unsucess           |
|--------------------|--------------------------| 
|      Done          |       AuthError          |
|                    |  InternalServerError     |
|                    |    PermissionDenied      |
```
```json
{"Header": {"RequestID": 'int',
            "AuthKey": 'string',
            "Verb": "EndSession"},
 "Body" : {}}
```
> Example:
>> Request: 
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "AuthKey": "KLEabctgejh2Ayhe3IUEND",
>>            "Verb": "EndSession"},
>> "Body" : {}} 
>> ```
>> Reply:
>> ``` json
>> {"Header": {"RequestID": 145284,
>>            "Verb": "Done"},
>> "Body" : {} 
>> ```

Signals
=====
NewMsg
---
when you or other member of session call publish msg api all users receive this signal <br>
and you must insert this message in message timeline <br>
Note : <br>
PublisherID => if  PublisherID = YOURUSERID this message published by you otherwise this message publish by PublisherName
```json
{"Header": {"Verb": "NewMsg"},
 "Body" : {
         "MsgHeader": {"MsgType": 'TEXT',
                       "PublisherID" : 'string',
                       "PublisherName": 'string',
                       "TimeStamp":  'int'},
         "MsgBody": 'string'
 }}
```
> Example:
>> Signal: 
>> ``` json
>> {"Header": {"Verb": "NewMsg"},
>>  "Body" : {
>>         "MsgHeader": {"MsgType": "TEXT",
>>                       "PublisherID" : "5d0269b050f324328dd6423b",
>>                       "PublisherName": "ali",
>>                       "TimeStamp":  1564227547323},
>>         "MsgBody": "hi i am there, can you hear me?"
>> }}
>> >> ```

ChangeMemberStatus
----
if every users join or leave in session you receive this signal <br>
Note: <br>
Status => show this user has connection with websocket server or not <br> 
ConferenceStatus => show this user are joined to voice conference or not

```json
{"Header": {"Verb": "ChangeMemberStatus"},
 "Body" : {
         "UserID": 'string',
         "Status": 'ONLINE | OFFLINE',
         "ConferenceStatus": 'ONLINE | OFFLINE'
 }}
```
> Example:
>> Signal: 
>> ``` json
>> {"Header": {"Verb": "ChangeMemberStatus"},
>> "Body" : {
>>         "UserID": "5d0269b050f324328dd6423b",
>>         "Status": "ONLINE",
>>         "ConferenceStatus": "OFFLINE"
>> }}
>> ```

ChangeSessionStatus
----
if main_reviewer call FormalizeSession api , every user in session receive this signal<br>
and recording parameters show this session is recording now 
```json
{"Header": {"Verb": "ChangeSessionStatus"},
 "Body" : {
         "Recording": 'boolean'
 }}
```
> Example:
>> Signal: 
>> ``` json
>> {"Header": {"Verb": "ChangeSessionStatus"},
>> "Body" : {
>>         "Recording": true
>> }}
>> ```


NewActiveDoc
----
if RESEARCHER call ChangeActiveDoc Api every user in session receive this signal <br>
DocID parameter show Active Doc image for presentation
```json
{"Header": {"Verb": "NewActiveDoc"},
 "Body" : {
         "DocID": 'string'
 }}
```
> Example:
>> Signal: 
>> ``` json
>> {"Header": {"Verb": "NewActiveDoc"},
>> "Body" : {
>>         "DocID": "example doc id"
>> }}
>> ```


SessionEnded
----
if main_reviewer call api EndSession every user in session receive this signal<br>
when client receive this signal must close connection with websocket server and sip server <br>
otherwise these connection closed by server after 2 sec
```json
{"Header": {"Verb": "SessionEnded"},
 "Body" : {}}
```
> Example:
>> Signal: 
>> ``` json
>> {"Header": {"Verb": "SessionEnded"},
>> "Body" : {}
>> }
>> ```


